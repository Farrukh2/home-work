package database;

import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.sql.*;

public class Main {

    private static final String URL = "jdbc:mysql://localhost:3306/mydbtest?autoReconnect=true&useSSL=false";
    private static final String Username = "root";
    private static final String Password = "12345";

    public static void main(String[] args)
    {
        try {


            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);


        } catch (SQLException e) {
            e.printStackTrace();
        }

        /*try(Connection connection = DriverManager.getConnection(URL,Username,Password); Statement statement = connection.createStatement())
        {
            statement.execute("insert into users(name, age, email) values('Second', 80, 'world@email')");
        } catch (SQLException e) {
            e.printStackTrace();
        }*/

        /*try(Connection connection = DriverManager.getConnection(URL,Username,Password); Statement statement = connection.createStatement())
        {
            statement.executeUpdate("update users set name='Anton', age = 45 where id = 4;");
        } catch (SQLException e) {
            e.printStackTrace();
        }*/

        try(Connection connection = DriverManager.getConnection(URL,Username,Password); Statement statement = connection.createStatement())
        {
            ResultSet res = statement.executeQuery("select * from users");

            while (res.next())
            {
                int id = res.getInt(1);
                String name = res.getString(2);
                String age = res.getString(3);
                String email = res.getString(4);
                System.out.print(id + "\t");
                System.out.print(name + "\t");
                System.out.print(age + "\t");
                System.out.println(email);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        /*try(Connection connection = DriverManager.getConnection(URL,Username,Password); Statement statement = connection.createStatement())
        {
            statement.addBatch("insert into users(name, age, email) values('Barry', 45, 'barry@email');");
            statement.addBatch("insert into users(name, age, email) values('Larry', 45, 'larry@email');");
            statement.addBatch("insert into users(name, age, email) values('Marry', 45, 'marry@email');");

            statement.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
        }*/
    }
}
